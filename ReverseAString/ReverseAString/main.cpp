#include<iostream>
#include<vector>

using namespace std;

class StringReverse
{
public:
	   string reverseString(string str)
	   {
           int len = (int)str.length() - 1;
           int i   = 0;
           while(i < len)
           {
               char temp = str[i];
               str[i]    = str[len];
               str[len]  = temp;
               i++;
               len--;
           }
           return str;
       }
};

int main()
{
    string str = "this is a string";
    StringReverse sR = StringReverse();
    int len = (int)str.length();
    string newStr = "";
    string opString = "";
    for(int i = 0 ; i < len ; i++)
    {
        if(str[i] == ' ')
        {
            opString += " " + sR.reverseString(newStr);
            newStr    = "";
        }
        else
        {
            newStr += str[i];
        }
    }
    
    opString += " " + sR.reverseString(newStr);
    cout<<sR.reverseString(opString)<<endl;
    return 0;
}
